<?php

class UserTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */


    public function test_User_Can_Be_Registered() {
        $input = array(
            "name" => "Niels Vermeiren",
            "password" => "MyPassword",
            "passwordAgain" => "MyPassword",
            "email" => "niels@ucll.be"
        );
        $response = $this->action('POST', 'UserController@register', $input);
        $this->assertViewHas('success');

    }

	public function test_can_visit_homepage_example()
	{
		$crawler = $this->client->request('GET', '/');
		$this->assertTrue($this->client->getResponse()->isOk());
	}




}
