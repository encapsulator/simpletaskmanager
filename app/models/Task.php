<?php
class Task extends BaseModel
{
    protected $table = 'tasks';
    protected $fillable = array( 'name', 'priority', 'date', 'groupid', 'userid', 'completed');
    protected $guarded = array('id');

    public function group () {
        return $this->hasOne('Group');
    }

    public function modify( $input ) {
        $this->store($input);
    }

    public function store($input) {
        $this->name = $input['name'];
        $this->priority = $input['priority'];
        $this->date = $input['date'];
        $this->userid = Auth::user()->id;
        $this->completed = 0;
    }
}