<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends BaseModel implements UserInterface, RemindableInterface {
	use UserTrait, RemindableTrait;

	protected $table = 'users';
	protected $hidden = array('password', 'remember_token');
    protected $guarded = array('id', 'password', 'remember_token');
    protected $fillable = array('name', 'email', 'salt');

    public function tasks() {
        return $this->hasMany('Task');
    }

    public function getAuthPassword() {
        return $this->password;
    }

    public function getAuthIdentifier() {
        return $this->getKey();
    }

    public function getRememberToken() {
        return $this->rememtok;
    }

    public function getRememberTokenName() {
        return 'rememtok';
    }

    public function setRememberToken($value) {
        $this->rememtok = $value;
    }

    public function store($input) {
        $this->name = $input['name'];
        $this->email = $input['email'];
        $this->password = Hash::make($input['password']);
    }

    public function modify($input) {
        make($input);
    }

}
