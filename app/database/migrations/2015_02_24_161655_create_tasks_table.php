<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('tasks', function($table){
            $table->increments('id');
            $table->string('name');
            $table->integer('priority');
            $table->date('date');


            $table->unsignedInteger('userid');
            $table->foreign('userid')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->boolean('completed');
            $table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks');
	}

}
