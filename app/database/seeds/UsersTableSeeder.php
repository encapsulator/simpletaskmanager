<?php

class UsersTableSeeder extends Seeder {

    public function run() {
        DB::table('users')->delete();
        $now = date('Y-m-d H:i:s');
        DB::table('users')->insert(array(
            array(
                'name' => 'Niels Vermeiren',
                'email' => 'vermeiren_niels@hotmail.com',
                'password' => \Hash::make('password'),
                'created_at' => $now,
                'updated_at' => $now
            ),
            array(
                'name' => 'Admin',
                'email' => 'Admin@admin.com',
                'password' => \Hash::make('admin'),
                'created_at' => $now,
                'updated_at' => $now
            )
        ));
    }
}
