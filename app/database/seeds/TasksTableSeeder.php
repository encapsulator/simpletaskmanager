<?php

class TasksTableSeeder extends Seeder {

    public function run() {
        DB::table('tasks')->delete();
        $now = date('Y-m-d H:i:s');

        $userid = DB::table('users')->where('name', '=', 'Admin')->get();


        DB::table('tasks')->insert(array(
            array(
                'name' => 'Vuilnis buiten zetten',
                'priority' => 8,
                'date' => '2015-01-01',
                'userid' => $userid[0]->id,
                'completed' => false,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array(
                'name' => 'Wiskunde taak',
                'priority' => 5,
                'date' => $now,
                'userid' => $userid[0]->id,
                'completed' => true,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array(
                'name' => 'Internetprogrammeren website',
                'priority' => 10,
                'date' => $now,
                'userid' => $userid[0]->id,
                'completed' => false,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array(
                'name' => 'Besturingssystemen test',
                'priority' => 6,
                'date' => $now,
                'userid' => $userid[0]->id,
                'completed' => true,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array(
                'name' => 'Afwassen',
                'priority' => 3,
                'date' => $now,
                'userid' => $userid[0]->id,
                'completed' => false,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array(
                'name' => 'CV opstellen',
                'priority' => 1,
                'date' => $now,
                'userid' => $userid[0]->id,
                'completed' => true,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array(
                'name' => 'Portfolio samenstellen',
                'priority' => 5,
                'date' => $now,
                'userid' => $userid[0]->id,
                'completed' => false,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array(
                'name' => 'Website maken',
                'priority' => 2,
                'date' => $now,
                'userid' => $userid[0]->id,
                'completed' => true,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array(
                'name' => 'AJAX push patroon leren',
                'priority' => 7,
                'date' => $now,
                'userid' => $userid[0]->id,
                'completed' => false,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array(
                'name' => 'Oma bellen',
                'priority' => 10,
                'date' => $now,
                'userid' => $userid[0]->id,
                'completed' => true,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array(
                'name' => 'Lessenrooster importeren',
                'priority' => 5,
                'date' => $now,
                'userid' => $userid[0]->id,
                'completed' => false,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array(
                'name' => 'Virussen verwijderen',
                'priority' => 3,
                'date' => $now,
                'userid' => $userid[0]->id,
                'completed' => true,
                'created_at' => $now,
                'updated_at' => $now
            )
        ));
    }
}
