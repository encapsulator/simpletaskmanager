<?php
/**
 * Created by PhpStorm.
 * User: niels
 * Date: 4/03/2015
 * Time: 21:34
 */
use validation\ValidationException;
class TaskController extends BaseController {
    private $taskRepository;

    public function __construct(\repository\TaskRepository $taskRepos ) {
        $this->taskRepository = $taskRepos;
    }

    public function store() {
        $result = null;
        try {
            $result = $this->taskRepository->store(Input::all());
        } catch (ValidationException $e) {
            $errors = explode("&", $e->getMessage());
            return View::make('tasks', array(
                "errors" => $errors,
                "input" => Input::all(),
                "tasks" => $this->getTasks(),
                "action" => 'stored'
            ));
        }
        if($result) {
            return Redirect::to('/tasks');
        }
    }

    public function update($id) {
        $result = null;
        try {
            $result = $this->taskRepository->update(Input::all(), $id);
        } catch (ValidationException $e) {
            $errors = explode("&", $e->getMessage());
            return View::make('tasks', array(
                "errors" => $errors,
                "tasks" => $this->getTasks(),
                "input" => Input::all(),
                "action" => "updated"
            ));
        }
        if($result) {
            return Redirect::to('/tasks');
        }
    }

    public function destroy($id) {
        if($this->taskRepository->delete($id)) {
            return Response::json( array(
                'success' => true
            ), 200 );
        }
        return Response::json( array(
            'success' => false
        ), 404 );
    }

    private function getTasks() {
        return DB::table('tasks')->where('userid', Auth::user()->id)
                ->select('id','name', 'priority', 'date', 'completed')->paginate(10);
    }

    public function complete($id) {
        DB::table('tasks')->where('id', $id)->update(array('completed' => true));
        return Redirect::to('/');
    }

    public function toTasks() {
        return View::make('tasks', array(
            "tasks" => $this->getTasks()
        ));
    }

}