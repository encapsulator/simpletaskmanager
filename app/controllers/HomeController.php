<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function homePage()
	{
        if(Auth::check()) {
            $stats = $this->getStats();
            return View::make('home', array(
                "stats" => $stats,
                "tasks" => $this->getTasks()
            ));
        }
		return View::make('login');
	}

    private function getStats() {

        $total = DB::table('tasks')->where('userid', Auth::user()->id)->count();
        $finished = DB::table('tasks')->where('userid', Auth::user()->id)->where('completed', '=', 1)->count();
        $unfinished = DB::table('tasks')->where('userid', Auth::user()->id)->where('completed', '=', 0)->where('date', '<', date('Y-m-d'))->count();
        $pending = DB::table('tasks')->where('userid', Auth::user()->id)->where('completed', '=', 0)->where('date','>=', date('Y-m-d'))->count();

        return array(
            "total" => $total,
            "finished" => $total != 0? $finished * 100 / $total  : 0,
            "unfinished" => $total != 0? $unfinished * 100 / $total : 0,
            "pending" => $total != 0? $pending * 100/ $total : 0
        );
    }

    private function getTasks() {
        return DB::table('tasks')->where('userid', Auth::user()->id)->where('date', date('Y-m-d'))
            ->where('completed', 0)->orderBy('priority', 'desc')
            ->select('id','name', 'priority')->get();
    }

    public function toLogin()
    {
        return View::make('login');
    }

    public function toRegister()
    {
        return View::make('register');
    }



}
