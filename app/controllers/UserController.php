
<?php
use validation\ValidationException;

class UserController extends BaseController {

    private $userRepository;

    public function __construct(\repository\UserRepository $userRepos ) {
        $this->userRepository = $userRepos;
    }



    public function register() {
        $result = null;
        try {
            $result = $this->userRepository->store(Input::all());
        } catch (ValidationException $e) {
            $errors = explode("&", $e->getMessage());
            return View::make('register', array(
                "errors" => $errors,
                "input" => Input::all()
            ));
        }
        if($result) {
            return View::make('login', array('hoi' => 'true'));
        }
    }

    public function login()
    {
        $input = Input::all();
        $remember = $input["remember"];
        $creds = array (
            "email" => $input["email"],
            "password" => $input["password"]
        );
        if(\Auth::attempt($creds, $remember)) {
            return Redirect::to('/');
        } else {
            return View::make("login", array(
                "errors" => array("Incorrect combination email/password")
            ));
        }
    }

    public function logout() {
        \Auth::logout();
        return View::make('login');
    }



}
