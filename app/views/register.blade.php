<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Simple TaskManager | Register</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="{{ URL::asset('css/bootstrap.min.css'); }}" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="{{ URL::asset('css/styles.css'); }}" rel="stylesheet">
	</head>
	<body>
<!-- Header -->
<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Simple TaskManager</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        
        <li>
          <a href="{{ URL::to('/')  }}/register"><i class="glyphicon glyphicon-user"></i> Register</a>

        </li>
          @if (!Auth::check())
              <li><a href="{{ URL::to('/')  }}/login"><i class="glyphicon glyphicon-lock"></i> Login</a></li>
          @else
              <li><a href="{{ URL::to('/')  }}/user/logout"><i class="glyphicon glyphicon-lock"></i> Logout</a></li>
          @endif
      </ul>
    </div>
  </div><!-- /container -->
</div>
<!-- /Header -->

<!-- Main -->
<div class="container-fluid">
<div class="row">

   
      
  	</div><!-- /col-3 -->
    <div class="col-sm-6 col-sm-offset-3">
      	

              	<div class="panel panel-default">
                	<div class="panel-heading">
                      	<div class="panel-title">

                      	<h4>Register</h4>
                      	</div>
                	</div>
                	<div class="panel-body">
                        @if (sizeof($errors) != 0)
                            <div class="alert alert-danger">
                                @foreach ($errors as $error)
                                    <p>{{$error}}</p>
                                @endforeach
                            </div>
                        @endif

                      <form class="form form-vertical" method="POST" action="{{URL::to('/')}}/user/register">

                          <div class="control-group">
                              <label>Name</label>
                              <div class="controls">
                                  <input type="text" name="name" class="form-control" placeholder="Enter your full name">
                              </div>
                          </div>

                          <div class="control-group">
                          <label>E-mail</label>
                          <div class="controls">
                           <input type="email" name="email" class="form-control" placeholder="Enter your email address">
                          </div>
                        </div>


                        <div class="control-group">
                          <label>Password</label>
                          <div class="controls">
                             <input type="text" name="password" class="form-control" placeholder="*****" >
                          </div>
                        </div>

                          <div class="control-group">
                              <label>Re-enter Password</label>
                              <div class="controls">
                                  <input type="text" name="passwordAgain" class="form-control" placeholder="*****" >
                              </div>
                          </div>


                        <div class="control-group">
                        	<label></label>
                        	<div class="controls">
                        	<button type="submit" class="btn btn-primary">
                              Register
                            </button>
                        	</div>
                        </div>   
                        
                      </form>
                
                
                  </div><!--/panel content-->
                </div><!--/panel-->
              

              
			</div><!--/col-span-12 -->
     
      </div><!--/row-->
      

      
     
  	</div><!--/col-span-9-->
</div>
</div>
<!-- /Main -->

  
	<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scripts.js"></script>
	</body>
</html>