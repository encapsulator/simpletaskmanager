<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Simple TaskManager</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="{{ URL::asset('css/bootstrap.min.css'); }}" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="{{ URL::asset('css/styles.css'); }}" rel="stylesheet">
	</head>
	<body>
<!-- Header -->
<div id="wrapcontainer">
<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Simple TaskManager</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        
        <li class="dropdown">
          <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><i class="glyphicon glyphicon-user"></i> Admin <span class="caret"></span></a>
          <ul id="g-account-menu" class="dropdown-menu" role="menu">
            <li><a href="#">My Profile</a></li>
          </ul>
        </li>
          @if (!Auth::check())
              <li><a href="{{ URL::to('/')  }}/login"><i class="glyphicon glyphicon-lock"></i> Login</a></li>
          @else
              <li><a href="{{ URL::to('/')  }}/user/logout"><i class="glyphicon glyphicon-lock"></i> Logout</a></li>
          @endif
      </ul>
    </div>
  </div><!-- /container -->
</div>
<!-- /Header -->

<!-- Main -->
<div class="container-fluid" id="main">
<div class="row">
	<div class="col-sm-3">
      <!-- Left column -->
  
           
      <hr>
      
      <strong> Navigation </strong>
      
      <hr>
      
      <ul class="nav nav-pills nav-stacked">
        <li class="nav-header"></li>
          <li><a href="{{URL::to('/')}}"><i class="glyphicon glyphicon-dashboard"></i> Dashboard</a></li>
        <li><a href="{{URL::to('/')}}/tasks"><i class="glyphicon glyphicon-list"></i> Tasks</a></li>

      </ul>
      
   
      
  	</div><!-- /col-3 -->
    <div class="col-sm-9">
      	
      <!-- column 2 -->	

     <strong><i class="glyphicon glyphicon-dashboard"></i> Dashboard</strong>
      
      	<hr>

		<div class="row">
           
            
          
            <!-- center left-->	
         	<div class="col-md-6">
			  <div class="well">Pending Tasks <span class="badge pull-right">{{sizeOf($tasks)}}</span></div>
              

              <div class="panel panel-default">
                  <div class="panel-heading"><h4>Today's tasks</h4></div>
                  <div class="panel-body">
                      <table class="table table-striped">
                          <thead>
                          <tr><th>Name</th><th>Priority</th><th>Complete</th></tr>
                          </thead>
                          <tbody>
                          @foreach ($tasks as $task)
                            <tr><td>{{$task->name}}</td><td>{{$task->priority}}</td><td><a href="{{URL::to('/')}}/tasks/complete/{{$task->id}}" class="glyphicon glyphicon-ok"></a></td></tr>
                          @endforeach
                          </tbody>
                      </table>
                  </div>
              	</div>


                <hr>


   
          	</div><!--/col-->
        	<div class="col-md-6">
				
                 <div class="panel panel-default">
                  <div class="panel-heading"><h4>Statistics</h4></div>
                  <div class="panel-body">
                    
                    <small>Completed</small>
                    <div class="progress">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $stats["finished"] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $stats["finished"] }}%">
                        <span class="sr-only">{{ $stats["finished"] }}% Complete</span>
                      </div>
                    </div>
                    <small>Pending</small>
                    <div class="progress">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{ $stats["pending"] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $stats["pending"] }}%">
                        <span class="sr-only">{{ $stats["pending"] }}% Complete</span>
                      </div>
                    </div>
                    <small>Unfinished</small>
                    <div class="progress">
                      <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{ $stats["unfinished"] }}" aria-valuemin="0" aria-valuemax="{{ $stats["unfinished"] }}" style="width: {{$stats["unfinished"]}}%">
                        <span class="sr-only">{{ $stats["unfinished"] }}% Complete (warning)</span>
                      </div>
                    </div>


                  </div><!--/panel-body-->
              </div><!--/panel-->

              

                

              
			</div><!--/col-span-6-->
     
      </div><!--/row-->
      

     
  	</div><!--/col-span-9-->

</div>
<!-- /Main -->
</div>
<footer class="text-center">&copy; <a href="http://r0449141.webontwerp.khleuven.be/portfolio/" target="_blank"><strong>Niels Vermeiren</strong></a></footer>


</div>

  
	<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scripts.js"></script>
	</body>
</html>