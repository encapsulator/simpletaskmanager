<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Simple TaskManager</title>
    <meta name="generator" content="Bootply" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="{{ URL::asset('css/bootstrap.min.css'); }}" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->
    <link href="{{ URL::asset('css/styles.css'); }}" rel="stylesheet">
</head>
<body>
<script>
    $( window ).load(function() {
        $('body').on('click', '.editTask', function() {
            var parent = $(this).parent().parent();
            var elements = parent.find('td');

            $(".editForm").find('#editTaskName').val(elements.eq(1).html());
            $(".editForm").find('#editTaskPriority').val(elements.eq(2).html());
            $(".editForm").find('#editTaskDate').val(elements.eq(3).html());
            var a = "{{URL::to('/')}}/tasks/" + elements.eq(0).html();
            $('.editForm').attr('action', a);

        });

        $("body").on('click', '.ajaxRemove',function() {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open('DELETE', $(this).attr('data-url'), true);
            xmlHttp.send();
            xmlHttp.onreadystatechange = function() {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    $("#wrapcontainer").load('{{URL::to('/')}}/tasks #wrapcontainer');
                }
            }
        });
    });
</script>
<!-- Header -->
<div id="wrapcontainer">
    <div id="top-nav" class="navbar navbar-inverse navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Simple TaskManager</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown">
                        <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><i class="glyphicon glyphicon-user"></i> Admin <span class="caret"></span></a>
                        <ul id="g-account-menu" class="dropdown-menu" role="menu">
                            <li><a href="#">My Profile</a></li>
                        </ul>
                    </li>
                    @if (!Auth::check())
                        <li><a href="{{ URL::to('/')  }}/login"><i class="glyphicon glyphicon-lock"></i> Login</a></li>
                    @else
                        <li><a href="{{ URL::to('/')  }}/user/logout"><i class="glyphicon glyphicon-lock"></i> Logout</a></li>
                    @endif
                </ul>
            </div>
        </div><!-- /container -->
    </div>
    <!-- /Header -->

    <!-- Main -->
    <div class="container-fluid" id="main">
        <div class="row">
            <div class="col-sm-3">
                <!-- Left column -->


                <hr>

                <strong> Navigation </strong>

                <hr>

                <ul class="nav nav-pills nav-stacked">
                    <li class="nav-header"></li>
                    <li><a href="{{URL::to('/')}}"><i class="glyphicon glyphicon-dashboard"></i> Dashboard</a></li>
                    <li><a href="{{URL::to('/')}}/tasks"><i class="glyphicon glyphicon-list"></i> Tasks</a></li>

                </ul>



            </div><!-- /col-3 -->
            <div class="col-sm-9">


                <strong><i class="glyphicon glyphicon-dashboard"></i> Dashboard</strong>

                <hr>

                <div class="row">



                    <!-- center left-->
                    <div class="col-md-9">



                        <div class="panel panel-default">
                            <div class="panel-heading"><h4>Tasks</h4></div>
                            <div class="panel-body">
                                @if (sizeof($errors) != 0)
                                    <div class="alert alert-danger">
                                        @foreach ($errors as $error)
                                            <p>{{$error}}</p>
                                        @endforeach
                                    </div>
                                @endif
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Priority</th>
                                        <th>Date</th>
                                        <th>Edit</th>
                                        <th>Remove</th>
                                        <th>Completed</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($tasks as $task)
                                        <tr>
                                            <td>{{$task->id}}</td>
                                            <td class="taskName">{{$task->name}}</td>
                                            <td>{{$task->priority}}</td>
                                            <td>{{$task->date}}</td>
                                            <td><a data-toggle="modal" href="#editTaskModal" class="glyphicon glyphicon-pencil editTask"></a></td>
                                            <td>

                                                <a data-url="{{URL::to('/') . "/tasks/" . $task->id}}" href="#" class="glyphicon glyphicon-remove ajaxRemove"></a>

                                            </td>
                                            @if($task->completed == 1)
                                                <td><i class="glyphicon glyphicon-ok"></i></td>
                                            @else
                                                <td><i class="glyphicon glyphicon-remove"></i></td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    <?php echo $tasks->links(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <hr>
                    </div><!--/col-->
                    <div class="col-md-3">

                        <div class="btn-group btn-group-justified">
                            <a  data-toggle="modal" href="#addTaskModal" class="btn btn-primary col-sm-3">
                                <i class="glyphicon glyphicon-plus"></i><br>
                                New Task
                            </a>

                        </div>
                    </div><!--/col-span-6-->

                </div><!--/row-->



            </div><!--/col-span-9-->

        </div>
        <!-- /Main -->
    </div>
    <footer class="text-center">&copy; <a href="http://r0449141.webontwerp.khleuven.be/portfolio/" target="_blank"><strong>Niels Vermeiren</strong></a></footer>

    <div class="modal" id="addTaskModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Add Task</h4>
                </div>
                <form class="form form-vertical" method="post" action="{{URL::to('/')}}/tasks">
                <div class="modal-body">

                        <div class="control-group">
                            <label>Name</label>
                            <div class="controls">
                                <input type="text" name="name" class="form-control" value="@if( sizeof($errors)!= 0&&$action=='stored'){{$input["name"]}}@endif" >
                            </div>
                        </div>


                        <div class="control-group">
                            <label>Priority</label>
                            <div class="controls">
                                <input type="text" name="priority" class="form-control" value="@if(sizeof($errors)!= 0&&$action=='stored'){{$input["priority"]}}@endif" >
                            </div>
                        </div>

                    <div class="control-group">
                        <label>Date</label>
                        <div class="controls">
                            <input type="text" name="date" class="form-control" placeholder="format: yyyy-mm-dd " value="@if(sizeof($errors)!=0&&$action=='stored'){{$input["date"]}}@endif">
                        </div>
                    </div>



                </div>
                <div class="modal-footer">
                    <a href="#" data-dismiss="modal" class="btn">Close</a>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dalog -->
    </div><!-- /.modal -->
    <div class="modal" id="editTaskModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Edit Task</h4>
                </div>

                {{Form::open(array('method' => 'put', 'class' => 'form form-vertical editForm'))}}
                    <div class="modal-body">

                        <div class="control-group">
                            <label>Name</label>
                            <div class="controls">
                                <input id="editTaskName" type="text" name="name" class="form-control">
                            </div>
                        </div>


                        <div class="control-group">
                            <label>Priority</label>
                            <div class="controls">
                                <input id="editTaskPriority" type="text" name="priority" class="form-control" >
                            </div>
                        </div>

                        <div class="control-group">
                            <label>Date</label>
                            <div class="controls">
                                <input id="editTaskDate" type="text" name="date" class="form-control" placeholder="format: yyyy-mm-dd " >
                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">
                        <a href="#" data-dismiss="modal" class="btn">Close</a>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dalog -->
    </div><!-- /.modal -->
</div>


    <!-- script references -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>

</body>
</html>