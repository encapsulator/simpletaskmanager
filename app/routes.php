<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', "HomeController@homePage" );
Route::get('/login', 'HomeController@toLogin');
Route::get('/register', 'HomeController@toRegister');

Route::group(array('prefix' => 'user'), function()
{
        Route::post('/login', 'UserController@login');
        Route::post('/register', 'UserController@register');

    Route::group(array('before' => 'auth'), function(){
        Route::get('/logout', 'UserController@logout');
        //RESTful controller
        Route::resource('', 'UserController', array('only' => array('update', 'show')));
    });
});

//RESTful controller
Route::group(array("before" => "auth"), function() {
    Route::resource('tasks', 'TaskController', array('only' => array('update', 'store', 'destroy')));
});

Route::group(array('prefix' => 'tasks', 'before' => 'auth'), function() {
    Route::get('', 'TaskController@toTasks');
    Route::get('/complete/{id}', 'TaskController@complete');
});