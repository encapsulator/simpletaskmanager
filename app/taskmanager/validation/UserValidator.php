<?php
/**
 * Created by PhpStorm.
 * User: niels
 * Date: 2/03/2015
 * Time: 22:26
 */

namespace validation;


class UserValidator extends BaseModelValidator {
    protected $rules = array (
        "name" => "required",
        "email" => "required|email|unique:users,email",
        "password" => "required",
        "passwordAgain" => "required|same:password"
    );

    protected $messages = array (
        "name.required" => "Please enter your name",
        "email.required" => "The email field is required",
        "email.email" => "The email field is not a valid email address",
        "email.unique" => "There exists already a user with this email address",
        "password.required" => "The password field is required",
        "passwordAgain.required" => "Please re-enter your password",
        "passwordAgain.same" => "The password fields did not match"
    );

}