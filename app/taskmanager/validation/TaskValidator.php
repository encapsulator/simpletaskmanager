<?php
/**
 * Created by PhpStorm.
 * User: niels
 * Date: 4/03/2015
 * Time: 20:27
 */

namespace validation;


class TaskValidator extends BaseModelValidator {
    protected $rules = array (
        "name" => "required",
        "priority" => "required|integer|between:1,10",
        "date" => "required|date"
    );

    protected $messages = array(
        "name.required" => "Please enter the name of the task",
        "priority.required" => "Please enter a priority for the task",
        "priority.integer" => "The priority field was not of the type integer",
        "priority.between" => "The priority value must be between 1 and 10",
        "date.required" => "Please specify a date",
        "date.date" => "The date field doesn't contain a valid date",
    );
}