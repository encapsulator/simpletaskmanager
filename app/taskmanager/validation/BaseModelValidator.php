<?php
namespace validation;
class BaseModelValidator
{
    protected $attributes;
    protected $rules;
    protected $messages;
    protected $validator;
    public function setAttributes( $attributes )
    {
        $this->attributes = $this->_preProcessAttributes( $attributes );
    }
    public function make( )
    {
        if ( !$this->validator ) {
            $this->validator = \Validator::make( $this->attributes, $this->rules, $this->messages );
        }
        return $this->validator;
    }
    public function fails( )
    {
        if ( !$this->validator ) {
            $this->make();
        }
        return $this->validator->fails();
    }
    public function passes( )
    {
        if ( !$this->validator ) {
            $this->make();
        }
        return $this->validator->passes();
    }
    public function getErrors( )
    {
        return $this->validator->errors()->all();
    }
    protected function _preProcessAttributes( $attributes )
    {
        return $attributes;
    }
}
