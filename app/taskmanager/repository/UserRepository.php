<?php
namespace repository;

use validation\ValidationException;
class UserRepository extends BaseRepository
{
    protected $validators = array( "userValidator" => "validation\\UserValidator" );

    public function __construct( \User $userModel)
    {
        $this->model = $userModel;
    }
    public function store( $input )
    {
        $this->validator = \App::make( $this->getValidator( 'userValidator' ) );
        $result          = parent::store( $input );
        if ( null !== $this->messageBag ) {
            throw new ValidationException( implode( "&", $this->messageBag ) );
        }
        return $result;
    }
    public function update( $input, $id )
    {
        $this->validator = \App::make( $this->getValidator( 'update' ) );
        $result          = parent::update( $id, $input );
        if ( null !== $this->messageBag ) {
            throw new ValidationException( implode( "&", $this->messageBag ) );
        }
        return $result;
    }

    public function delete( $id )
    {
        return parent::delete($id);
    }

}