<?php
namespace repository;

class BaseRepository
{
	protected $validator;
	protected $messageBag;
	protected $model;
	protected $user;

	public function find( $id )
	{
		$class = get_class( $this->model );
		return $class::find( $id );
	}
	public function update( $id, $input )
	{
		$this->setValidationInput( $input );
		if ( $this->validator->passes() ) {
			$object = $this->find( $id );
			try {
				$object->modify( $input );
				$object->save();
				return true;
			}
			catch ( Exception $e ) {
				$this->messageBag = null;
				return false;
			}
		} 
		$this->messageBag = $this->validator->getErrors();
		return false;
	}
	public function delete( $id )
	{
		try {
			$object = $this->find( $id );
			$object->delete();
			return true;
		}
		catch ( Exception $e ) {
			return false;
		}
	}
	public function store( $input )
	{
		$this->setValidationInput( $input );

		if ( $this->validator->passes() ) {

			try {
				$class  = get_class( $this->model );
				$object = new $class;

				$object->store( $input );
				$object->save();
				$this->messageBag = null;
				return true;
			}
			catch ( Exception $e ) {
				$this->messageBag = null;
				return false;
			}
		}
		$this->messageBag = $this->validator->getErrors();
		return false;
	}
	protected function setValidationInput( $input )
	{
		$this->validator->setAttributes( $input );
	}
	protected function getMessageBag( )
	{
		return $this->messageBag;
	}
	protected function validate( $input )
	{
		$this->setValidationInput( $input );
		if ( $this->validator->passes() ) {
			return true;
		}
		$this->messageBag = $this->validator->getErrors();
		return false;
	}
	protected function getValidator( $validationKey )
	{
		return $this->validators[ $validationKey ];
	}
}