<?php
/**
 * Created by PhpStorm.
 * User: niels
 * Date: 4/03/2015
 * Time: 20:22
 */

namespace repository;
use validation\ValidationException;

class TaskRepository extends BaseRepository {
    protected $validators = array( "taskValidator" => "validation\\TaskValidator" );

    public function __construct( \Task $taskModel)
    {
        $this->model = $taskModel;
    }
    public function store( $input )
    {
        $this->validator = \App::make( $this->getValidator( 'taskValidator' ) );
        $result          = parent::store( $input );
        if ( null !== $this->messageBag ) {
            throw new ValidationException( implode( "&", $this->messageBag ) );
        }
        return $result;
    }
    public function update( $input, $id )
    {
        $this->validator = \App::make( $this->getValidator( 'taskValidator' ) );
        $result          = parent::update( $id, $input );
        if ( null !== $this->messageBag ) {
            throw new ValidationException( implode( "&", $this->messageBag ) );
        }
        return $result;
    }

    public function delete( $id )
    {
        return parent::delete($id);
    }
}